from pyplays import Tasks

with Tasks() as t:
    for svc in ["e2scrub_reap.service", "systemd-resolved.service",
                "apt-daily-upgrade.timer", "apt-daily.timer",
                "e2scrub_all.timer", "fstrim.timer",
                "systemd-networkd.service"]:
        t.service("Disable unnecessary services",
            name=svc, state="stopped", enabled=False)
    t.systemd(name="systemd-udevd.service", masked=True)
    t.systemd(name="systemd-udev-trigger.service", masked=True)

    t.lineinfile(path="/etc/systemd/journald.conf",
        regexp=r"^#?SystemMaxUse *=.*",
        line="SystemMaxUse=100M")
    t.systemd(name="systemd-journald.service", state="restarted")

    t.import_role(name="container_static_network")
