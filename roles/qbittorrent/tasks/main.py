from pyplays import Args, Tasks

args = Args(vars, ["users"])

with Tasks() as t:
    t.import_role(name="pam_ldap")

    t.package("Install qBittorrent, headless version",
        name="qbittorrent-nox")

    for entry in args.users:
        t.command(_register="uid_cmd",
            argv=["id", "-u", entry['user']])

        t.file(
            path=f"/data/{entry['user']}/qBittorrent/config",
            state="directory",
            mode=0o700,
            owner=entry['user'])
        t.copy(f"Create service file for {entry['user']}",
            dest=f"/etc/systemd/system/qbittorrent-nox@{entry['user']}.service",
            content=f"""
            [Unit]
            Requires=nslcd.service
            After=nslcd.service

            [Service]
            ExecStart=/usr/bin/qbittorrent-nox --profile=/data/{entry['user']}
            User={entry['user']}
            Restart=always

            [Install]
            WantedBy=multi-user.target
            """)
        t.service(f"Stop for {entry['user']} so that we can replace the config",
            name=f"qbittorrent-nox@{entry['user']}",
            state="stopped")
        t.systemd(daemon_reload=True)
        t.template(f"Copy config for {entry['user']}",
            src="qbittorrent.conf.j2",
            dest=f"/data/{entry['user']}/qBittorrent/config/qBittorrent.conf",
            _vars={"save_path": entry['save_path']})
        t.service(f"Start for {entry['user']}",
            name=f"qbittorrent-nox@{entry['user']}",
            state="restarted",
            enabled=True)
