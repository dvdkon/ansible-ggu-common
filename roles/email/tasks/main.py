from pyplays import Tasks, Args

args = Args(vars, [
    "domains",
])

with Tasks() as t:
    t.file("Set /data permissions", name="/data", state="directory" ,mode=0o755)

    # Install OpenDKIM
    t.package(name="opendkim", state="latest")
    t.file(name="/data/dkim_keys", state="directory", group="opendkim", mode=0o750)
    # Generate DKIM key if it doesn't exist
    for domain in args.domains:
        t.file(name=f"/data/dkim_keys/{domain}",
            state="directory",
            group="opendkim",
            mode=0o750)
        t.command(
            cmd=f"opendkim-genkey --selector=default --bits=4096 --domain={domain} --directory=/data/dkim_keys/{domain}",
            creates=f"/data/dkim_keys/{domain}/default.txt")
        t.file(name=f"/data/dkim_keys/{domain}/default.private", owner="opendkim", mode=0o600)
        t.file(name="/var/run/opendkim", state="directory", mode=0o770)
        t.template(src="opendkim.conf.j2", dest="/etc/opendkim.conf")
        t.template(src="opendkim.KeyTable.j2", dest="/etc/opendkim.KeyTable")
        t.template(src="opendkim.SigningTable.j2", dest="/etc/opendkim.SigningTable")
        t.service(name="opendkim", state="reloaded", enabled=True)

    # Install Rspamd
    t.package(name=["rspamd", "redis"], state="latest")
    t.file(name="/data/redis", state="directory", group="redis", mode=0o770)
    t.copy(src="redis.conf", dest="/etc/redis.conf")
    t.service(name="redis", state="restarted", enabled=True)
    t.copy(src="milter_headers.conf", dest="/etc/rspamd/local.d/milter_headers.conf")
    t.copy(src="options.inc", dest="/etc/rspamd/local.d/options.inc")
    t.service(name="rspamd", state="reloaded", enabled=True)

    #Create virtmail user
    t.group(name="virtmail", gid=200)
    t.user(name="virtmail", uid=200)

    # Install cert copying workaround
    t.file(path="/etc/ssl/email-certs", state="directory")
    t.template(src="copy_certs.sh.j2", dest="/usr/local/bin/copy_certs.sh", mode=0o700)
    t.copy(src="copy_certs.service", dest="/etc/systemd/system/copy_certs.service")
    t.copy(src="copy_certs.timer", dest="/etc/systemd/system/copy_certs.timer")
    t.systemd(daemon_reload=True)
    t.service(name="copy_certs.timer", state="started", enabled=True)
    t.service(name="copy_certs.service", state="started")

    # Install Dovecot
    t.package(name=["dovecot-core", "dovecot-imapd", "dovecot-sieve", "dovecot-managesieved", "dovecot-ldap", "dovecot-lmtpd"], state="latest")
    t.template(src="dovecot.conf.j2", dest="/etc/dovecot/dovecot.conf")
    t.template(src="ldap.conf.ext.j2", dest="/etc/dovecot/ldap.conf.ext", mode=0o600)
    t.file(name="/data/dovecot", state="directory", owner="virtmail", mode=0o770)
    t.file(name="/etc/systemd/system/dovecot.service.d/", state="directory")
    t.copy(src="dovecot_groups.conf", dest="/etc/systemd/system/dovecot.service.d/groups.conf")
    t.systemd(daemon_reload=True)
    t.service(name="dovecot", state="reloaded", enabled=True)

    # Install postfix
    t.package(name=["postfix", "postfix-ldap"], state="latest")
    t.template(src="main.cf.j2", dest="/etc/postfix/main.cf")
    t.copy(src="master.cf", dest="/etc/postfix/master.cf")
    t.file(name="/data/postfix_queue", state="directory")
    t.file(name="/data/postfix_virtual", state="directory", group="virtmail", mode=0o770)
    t.file(name="/etc/postfix/ldap", state="directory")
    t.template(src="ldap/virt_mailboxes.cf.j2", dest="/etc/postfix/ldap/virt_mailboxes.cf", group="postfix", mode=0o640)
    t.template(src="ldap/virt_aliases.cf.j2", dest="/etc/postfix/ldap/virt_aliases.cf", group="postfix", mode=0o640)
    t.template(src="ldap/virt_senders.cf.j2", dest="/etc/postfix/ldap/virt_senders.cf", group="postfix", mode=0o640)
    t.user(name="postfix", append=True, groups=["shareddata", "opendkim", "dovecot"])
    t.systemd(daemon_reload=True)
    # First-run reload fails, because Postfix never starts?
    t.service(name="postfix@-", state="restarted", enabled=True)
