from pyplays import Tasks

with Tasks() as t:
    t.lineinfile(path="/etc/dnf/protected.d/systemd.conf",
        regexp="^systemd-udev$",
        state="absent")
    # TODO: Let users install these packages in the containers, maybe only run
    # when the container has been created?
    # Same with the services
    t.package("Remove unneeded packages",
        name=["rsyslog", "cronie", "systemd-udev", "NetworkManager"],
        state="removed")
    t.systemd(daemon_reload=True) # Will stop uninstalled services

    for svc in ["import-state.service", "loadmodules.service",
                "dnf-makecache.timer"]:
        t.service("Disable unnecessary services",
            name=svc, state="stopped", enabled=False)
    t.systemd(name="getty.target", masked=True)

    t.file(path="/etc/sysconfig/network-scripts/ifcfg-eth0",
        state="absent")

    t.lineinfile(path="/etc/systemd/journald.conf",
        regexp=r"^#?SystemMaxUse *=.*",
        line="SystemMaxUse=100M")
    t.systemd(name="systemd-journald.service", state="restarted")

    t.import_role(name="container_static_network")
