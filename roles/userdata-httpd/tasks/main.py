from pyplays import Args, Tasks

args = Args(vars, ["users"])

with Tasks() as t:
    t.import_role(name="pam_ldap")

    t.package(name="lighttpd", state="latest")
    t.template(
        src="lighttpd@.service",
        dest="/etc/systemd/system/lighttpd@.service")
    t.systemd(daemon_reload=True)

    t.file(
        path="/etc/lighttpd/host-extra",
        state="directory")

    for cfg in args.users:
        t.command(_register="uid_cmd",
            argv=["id", "-u", cfg["username"]])
        for site in cfg['sites']:
            t.file(
                path=f"/etc/lighttpd/host-extra/{site['hosts'][0]}.conf",
                state="touch")
        t.template(
            src="lighttpd.conf.j2",
            dest=f"/etc/lighttpd/{cfg['username']}.conf",
            _vars={
                "uid": "{{ uid_cmd.stdout }}",
                "httpd_user": cfg["username"],
                "sites": cfg["sites"],
            })
        t.service(
            name=f"lighttpd@{cfg['username']}",
            state="restarted",
            enabled=True)
