from pyplays import Tasks, Args

args = Args(vars, [
    "backup_sources", # List of name, UID pairs
])

with Tasks() as t:
    for src_name, src_uid in args.backup_sources:
        username = f"remote_backup_{src_name}"
        t.import_tasks(f"Create user {src_name}", "ggu-common/sidss_ro_user.py",
            _delegate_to="sidss",
            _vars=dict(
                uid=src_uid,
                username=username,
                password="", # Blank to prevent password login
                permissions=["sftp"]))

        t.file(path=f"/ldaphome/{username}/.ssh", state="directory", owner=src_uid)
        t.copy(
            src=f"secrets/backup_keys/{src_name}.pub",
            dest=f"/ldaphome/{username}/.ssh/authorized_keys",
            owner=src_uid,
            mode=0o600)
        t.file(path=f"/backup/{src_name}", state="directory", owner=src_uid, mode=0o700)
