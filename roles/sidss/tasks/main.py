from pyplays import Tasks

with Tasks() as p:
    p.package("Install system dependencies",
        name=["git", "libcap-dev", "stunnel", "gpg"],
        state="latest")

    p.apt_key(
        url="https://deb.nodesource.com/gpgkey/nodesource.gpg.key",
        state="present")
    p.apt_repository("Add NodeSource repo",
        repo="deb https://deb.nodesource.com/node_18.x {{ansible_distribution_release}} main",
        state="present")

    p.apt_key(
        url="https://dl.cloudsmith.io/public/caddy/stable/gpg.key",
        state="present")
    p.apt_repository("Add Caddy repo",
        repo="deb https://dl.cloudsmith.io/public/caddy/stable/deb/debian any-version main")

    p.package("Install third-party dependencies",
        name=["nodejs", "caddy"],
        state="latest")

    p.command("Install Yarn from Corepack",
        argv=["corepack", "enable"])

    p.command("Install node.js dependencies",
        "yarn global add typescript @types/node ts-node")

    p.command("Allow node.js to listen on low ports",
        "setcap cap_net_bind_service=+ep /usr/bin/node")

    p.file("Create SIDSS dir",
        path="/opt/sidss",
        state="directory")

    p.git("Get SIDSS source code",
        repo="https://gitlab.com/dvdkon/sidss/",
        version="ggu-prod",
        dest="/opt/sidss",
        force=True)

    p.user("Create SIDSS user",
        name="sidss",
        uid=500)

    for proj in ["common", "ldap_server", "mgmt_daemon", "http_server", "webui"]:
        p.command(f"Install node.js deps for {proj}",
            cmd="yarn install",
            chdir=f"/opt/sidss/{proj}")
        if proj == "common":
            p.command(f"Link comon",
                cmd="yarn link",
                chdir=f"/opt/sidss/{proj}")
        else:
            p.command(f"Link common for {proj}",
                cmd="yarn link sidss-common",
                chdir=f"/opt/sidss/{proj}")

    p.lineinfile("Set webui API location",
        path="/opt/sidss/webui/src/config.ts",
        regexp="^export const API_BASE = ",
        line='export const API_BASE = "https://" + (typeof location !== "undefined" ? location.hostname : "") + "/api";')

    p.command("Build webui",
        cmd="yarn run build",
        chdir="/opt/sidss/webui")

    p.copy("Copy Caddyfile",
        src="Caddyfile",
        dest="/etc/caddy/Caddyfile")

    p.file("Create SIDSS log dir",
        path="/var/log/sidss",
        state="directory",
        owner="sidss")

    p.file("Create SIDSS RO data dir",
        path="/data/sidss-mgmt-ro",
        state="directory",
        owner="sidss")
    for dir in ["users", "groups", "permissions", "services"]:
        p.file(f"Create SIDSS RO data dir - {dir}",
            path=f"/data/sidss-mgmt-ro/{dir}",
            state="directory",
            owner="sidss")

    p.file("Create SIDSS RW data dir",
        path="/data/sidss-mgmt-rw",
        state="directory",
        owner="sidss")

    p.file("Create SIDSS HTTP aux data dir",
        path="/data/sidss-http-aux",
        state="directory",
        owner="sidss")

    p.template("Copy SIDSS init script for the management daemon",
        src="sidss-mgmt-daemon.service.j2",
        dest="/etc/systemd/system/sidss-mgmt-daemon.service",
        mode=0o755)

    p.copy("Copy SIDSS service for the HTTP server",
        src="sidss-http-server.service",
        dest="/etc/systemd/system/sidss-http-server.service",
        mode=0o755)

    p.copy("Copy stunnel service file",
        src="stunnel@ldaps.service",
        dest="/etc/systemd/system/stunnel@ldaps.service")

    p.copy("Create timer to reload stunnel certificate",
        src="cert-reload.timer",
        dest="/etc/systemd/system/cert-reload.timer")

    p.copy(
        src="cert-reload.service",
        dest="/etc/systemd/system/cert-reload.service")

    p.systemd(daemon_reload=True)

    p.service("Start and enable SIDSS management daemon",
        name="sidss-mgmt-daemon",
        state="restarted",
        enabled=True)

    p.service("Start and enable SIDSS HTTP server",
        name="sidss-http-server",
        state="restarted",
        enabled=True)

    p.service("Start and enable Caddy",
        name="caddy",
        state="reloaded",
        enabled=True)

    p.template("Copy stunnel config file",
        src="stunnel.conf.j2",
        dest="/etc/stunnel/ldaps.conf")

    p.user("Add stunnel user to shareddata group",
        name="stunnel",
        append=True,
        groups=["shareddata"])

    p.service("Start and enable stunnel (for LDAPS)",
        state="restarted",
        name="stunnel@ldaps",
        enabled=True)

    p.service(name="cert-reload.timer",
        state="restarted",
        enabled=True)
