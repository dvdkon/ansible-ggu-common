from pyplays import Tasks

with Tasks() as t:
    t.copy(dest="/usr/local/bin/setup-lxd-network",
        mode=0o755,
        content="""#!/bin/sh
ip addr flush dev eth0
ip route flush dev eth0
ip route flush default

ip link set up dev eth0
ip addr add dev eth0 {{ ip }}/24
ip route add default via {{ hostvars[lxd_host].ip }}
""")

    t.copy(dest="/etc/resolv.conf",
        content="""
nameserver {{ hostvars.dnsd.ip }}
nameserver 1.1.1.1
search {{ containers_domain }}
""")

    t.copy(dest="/etc/systemd/system/setup-lxd-network.service",
        content="""
[Unit]
Description="Set up LXD-provided eth0 interface"

[Service]
Type=oneshot
ExecStart=/usr/local/bin/setup-lxd-network

[Install]
WantedBy=network.target
WantedBy=basic.target
""")
    t.systemd(daemon_reload=True)
    t.service(name="setup-lxd-network", state="started", enabled=True)
