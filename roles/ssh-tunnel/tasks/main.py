from pyplays import Tasks
from textwrap import dedent

with Tasks() as t:
    t.import_role(name="pam_ldap", _vars={
        "ldap_base_dn_extra": ',pgm="ssh-tunnel;ldap-pgm-ssh-tunnel;6000",svc=ssh-tunnel',
    })
    t.import_role(name="pam_ldap_sshd")

    t.copy(dest="/etc/ssh/sshd_config",
        content=dedent("""
        HostKey /ssd_data/ssh_host_ed25519_key

        SyslogFacility AUTHPRIV

        PermitRootLogin no

        PasswordAuthentication yes
        ChallengeResponseAuthentication no
        UsePAM yes

        AllowAgentForwarding no
        AllowTcpForwarding yes
        GatewayPorts yes
        X11Forwarding no

        ForceCommand /bin/false

        AllowGroups ldap-pgm-ssh-tunnel
        """))
    t.command(
        cmd='ssh-keygen -t ed25519 -C "" -f /ssd_data/ssh_host_ed25519_key',
        creates="/ssd_data/ssh_host_ed25519_key")
    t.service(name="sshd", state="restarted", enabled=True)

