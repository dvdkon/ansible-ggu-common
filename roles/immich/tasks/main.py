from pyplays import Tasks, Args
from textwrap import dedent

args = Args(vars, ["instances"])

typesense_version = "0.24.1"
postgresql_version = "15"
immich_version = "v1.126.1"
imagemagick_url = "https://imagemagick.org/archive/ImageMagick-7.1.1-43.tar.xz"
libvips_url = "https://github.com/libvips/libvips/releases/download/v8.16.0/vips-8.16.0.tar.xz"
pgvectors_url = "https://github.com/tensorchord/pgvecto.rs/releases/download/v0.2.1/vectors-pg15_0.2.1_amd64.deb"

with Tasks() as t:
    t.stat(_register="nodesource_repo",
        path="/etc/apt/sources.list.d/nodesource.list")
    t.package(name="curl", state="latest")
    t.shell(cmd="curl -fsSL https://deb.nodesource.com/setup_20.x | bash -",
        _when="not nodesource_repo.stat.exists")

    t.package(name=[
            "git", f"postgresql-{postgresql_version}", "gcc", "g++", "meson",
            "ninja-build", "libheif-dev", "libglib2.0-dev", "libexpat1-dev",
            "nodejs", "python3-pip", "python3-venv", "redis", "ffmpeg",
            "gunicorn", "unzip", "libraw-dev", "libfftw3-dev", "libfftw3-dev",
            "libimagequant-dev", "libcgif-dev", "libexif-dev", "libwebp-dev",
            "libmatio-dev", "libopenexr-dev", "liborc-dev", "libltdl-dev",
        ], state="latest")

    t.get_url(
        dest="/root/typesense.deb",
        url=f"https://dl.typesense.org/releases/{typesense_version}/typesense-server-{typesense_version}-amd64.deb")
    t.apt(deb="/root/typesense.deb")
    t.command(_register="typesense_api_key_cmd",
        cmd="sed -nEe 's/^api-key *= *//p' /etc/typesense/typesense-server.ini")

    t.get_url(
        dest="/root/pgvectors.deb",
        url=pgvectors_url)
    t.apt(deb="/root/pgvectors.deb")
    t.lineinfile(path=f"/etc/postgresql/{postgresql_version}/main/postgresql.conf",
        regexp="^shared_preload_libraries *=",
        line="shared_preload_libraries = 'vectors.so'")
    t.lineinfile(path=f"/etc/postgresql/{postgresql_version}/main/postgresql.conf",
        regexp="^search_path *=",
        line="search_path = '\"$user\", public, vectors'")
    t.service(name="system-postgresql.slice", state="restarted")

    # TODO: Make sure build from source are optimised (-O2)
    t.get_url(dest="/root/imagemagick.tar.xz", url=imagemagick_url)
    t.unarchive(src="/root/imagemagick.tar.xz", dest="/root", remote_src=True)
    t.shell(chdir="/root",
        cmd="""
            version_existing=$(pkg-config --modversion ImageMagick)
            # Clean up old versions
            rm -r $(ls -d ImageMagick-* | sort -n | tail -n+2)
            version_downloaded=$(echo ImageMagick-* | sed -e 's/ImageMagick-//; s/-[0-9]*//')
            if [ "$version_existing" != "$version_downloaded" ]; then
                echo "Version mismatch ($version_existing, $version_downloaded), building ImageMagick..." >&2
                cd ImageMagick-*
                ./configure --with-modules
                make -j$(nproc)
                make install
                ldconfig
                cd ../..
            fi
        """)

    t.get_url(dest="/root/libvips.tar.xz", url=libvips_url)
    t.unarchive(src="/root/libvips.tar.xz", dest="/root", remote_src=True)
    t.shell(chdir="/root",
        cmd="""
            version_existing=$(pkg-config --modversion vips)
            # Clean up old versions
            rm -r $(ls -d vips-* | sort -n | tail -n+2)
            version_downloaded=$(echo vips-* | sed -e 's/vips-//')
            if [ "$version_existing" != "$version_downloaded" ]; then
                echo "Version mismatch ($version_existing, $version_downloaded), building libvips..." >&2
                cd vips-*
                # Disable TIFF support, because many RAW formats are based on
                # TIFF and it confuses libvips. This is what upstream does :(
                # TODO: Is this necessary?
                # Disable building ImageMagick support as a runtime-opened .so,
                # since then it's not considered (libvips bug?)
                meson setup build --prefix /usr/local -Dtiff=disabled -Dmagick-module=disabled
                cd build
                meson compile
                meson install
                cd ../..
            fi
        """)

    # Set up PG database (user per instance)
    t.file(path="/ssd_data", mode=0o755)
    t.file(path="/ssd_data/pg", state="directory", owner="postgres")
    t.file(path="/ssd_data/pg/data", state="directory", owner="postgres")
    t.lineinfile(_register="pg_conf_change",
        path=f"/etc/postgresql/{postgresql_version}/main/postgresql.conf",
        regexp="data_directory = '.*'",
        line="data_directory = '/ssd_data/pg/data'")
    t.stat(path="/ssd_data/pg/data/PG_VERSION", _register="pg_db_stat")
    t.command(
        _when="not pg_db_stat.stat.exists",
        _become=True, _become_user="postgres",
        cmd=f"/usr/lib/postgresql/{postgresql_version}/bin/initdb -E UTF8 -D /ssd_data/pg/data")
    t.service(name=f"postgresql@{postgresql_version}-main", state="restarted",
        _when="pg_conf_change is changed")

    t.git(dest="/opt/immich",
        repo="https://github.com/immich-app/immich",
        version=immich_version,
        depth=1,
        force=True)

    # Patch out extension creation, since we aren't running as superuser in the DB
    #t.lineinfile(path="/opt/immich/server/src/services/database.service.ts",
    #    regexp=r"^( *await this\.createVectorExtension\(\);)$",
    #    backrefs=True,
    #    line=r"//\1")

    # Needed to build sharp with system libvips
    t.command(cmd="npm install node-addon-api node-gyp", chdir="/opt/immich/server")
    t.command(cmd="npm install", chdir="/opt/immich/server")
    t.command(cmd="npm run build", chdir="/opt/immich/server")

    t.command(cmd="npm install", chdir="/opt/immich/open-api/typescript-sdk")
    t.command(cmd="npm run build", chdir="/opt/immich/open-api/typescript-sdk")

    t.command(cmd="npm install", chdir="/opt/immich/web")
    t.command(cmd="npm run build", chdir="/opt/immich/web")

    t.stat(_register="ml_venv", path="/opt/immich_ml_venv")
    t.command(cmd="python3 -m venv /opt/immich_ml_venv",
        _when="not ml_venv.stat.exists")
    t.shell(
        chdir="/opt/immich/machine-learning",
        cmd="""
            pip install --upgrade --break-system-packages poetry
            poetry config virtualenvs.create false
            . /opt/immich_ml_venv/bin/activate
            poetry install --sync --no-interaction --no-ansi --no-root --with cpu
        """)

    t.file(path="/opt/immich_data/geodata", state="directory")
    t.get_url(
        url="https://download.geonames.org/export/dump/cities500.zip",
        dest="/opt/immich_data/geodata/cities500.zip")
    t.get_url(
        url="https://download.geonames.org/export/dump/admin1CodesASCII.txt",
        dest="/opt/immich_data/geodata/admin1CodesASCII.txt")
    t.get_url(
        url="https://download.geonames.org/export/dump/admin2Codes.txt",
        dest="/opt/immich_data/geodata/admin2Codes.txt")
    t.get_url(
        url="https://raw.githubusercontent.com/nvkelso/natural-earth-vector/v5.1.2/geojson/ne_10m_admin_0_countries.geojson",
        dest="/opt/immich_data/geodata/ne_10m_admin_0_countries.geojson")
    t.command(cmd="unzip -o cities500.zip", chdir="/opt/immich_data/geodata")
    t.shell(cmd="date --iso-8601=seconds | tr -d '\\n' > /opt/immich_data/geodata/geodata-date.txt")

    t.file(state="link",
        src="/opt/immich/web/build",
        dest="/opt/immich_data/www")

    t.copy(dest="/usr/local/bin/immich-ml-run.sh", mode=0o755,
        content=dedent("""#!/usr/bin/env sh
            export IMMICH_HOST=$IMMICH_HOST_MACHINE_LEARNING
            export IMMICH_PORT=$IMMICH_PORT_MACHINE_LEARNING
            cd /opt/immich/machine-learning
            . /opt/immich_ml_venv/bin/activate
            exec ./start.sh
        """))

    t.copy(dest="/etc/systemd/system/immich-server@.service",
        content=dedent("""
        [Unit]
        Requires=postgresql@15-main.service

        [Service]
        User=immich-%i
        ExecStart=/usr/bin/env \\
            IMMICH_HOST=${IMMICH_HOST_SERVER} \\
            IMMICH_PORT=${IMMICH_PORT_SERVER} \\
            node /opt/immich/server/dist/main
        WorkingDirectory=/data/immich/%i
        EnvironmentFile=/data/immich/%i/env

        [Install]
        WantedBy=multi-user.target
        """))

    t.copy(dest="/etc/systemd/system/immich-machine-learning@.service",
        content=dedent("""
        [Unit]
        Requires=postgresql@15-main.service

        [Service]
        User=immich-%i
        ExecStart=/usr/local/bin/immich-ml-run.sh
        WorkingDirectory=/data/immich/%i
        EnvironmentFile=/data/immich/%i/env

        [Install]
        WantedBy=multi-user.target
        """))

    t.systemd(daemon_reload=True)

    t.shell(cmd=dedent(r"""
        psql <<'EOF'
        {# This will error out if the role/db exists. Ignore it. #}
        {% for name in instance_names %}
            {# TODO: Better per-instance password #}
            CREATE ROLE {{ name }} WITH LOGIN PASSWORD 'immich';
            CREATE DATABASE {{ name }} OWNER {{ name }};
            \c {{ name }}
            CREATE EXTENSION IF NOT EXISTS earthdistance CASCADE;
            CREATE EXTENSION IF NOT EXISTS vectors CASCADE;
            GRANT USAGE ON SCHEMA vectors TO {{ name }};
            GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA vectors TO {{ name }};
            GRANT SELECT ON ALL TABLES IN SCHEMA vectors TO {{ name }};
        {% endfor %}
        EOF
    """),
    _vars={"instance_names": [i["name"] for i in args.instances]},
    _become=True, _become_user="postgres")

    for inst in args.instances:
        name = inst["name"]
        t.user(name=f"immich-{name}", uid=1000 + inst["id"])
        t.file(path=f"/data/immich/{name}", state="directory", owner=f"immich-{name}", mode=0o700)
        # This is just terrible software engineering. Your production code
        # should not need to run from the source directory.
        t.file(dest=f"/data/immich/{name}/package.json", src="/opt/immich/server/package.json", state="link")
        t.file(dest=f"/data/immich/{name}/node_modules", src="/opt/immich/server/node_modules", state="link")
        t.file(dest=f"/data/immich/{name}/dist", src="/opt/immich/server/dist", state="link")
        # TODO: Make sure internal services are only reachable inside this container
        # (Ideally only by their user. This wouldn't be an issue if software
        # stopped using the loopback as a "secure network" and started using
        # HTTP over UNIX sockets... *grumble*)
        t.copy(dest=f"/data/immich/{name}/env", content=dedent(f"""
            DB_HOSTNAME=127.0.0.1
            DB_DATABASE_NAME={name}
            DB_USERNAME={name}
            DB_PASSWORD=immich
            REDIS_HOSTNAME=127.0.0.1
            TYPESENSE_HOST=127.0.0.1
            TYPESENSE_API_KEY=""" + "{{ typesense_api_key_cmd.stdout }}" + f"""
            NODE_ENV=production
            IMMICH_HOST_SERVER=0.0.0.0
            IMMICH_PORT_SERVER={10000 + inst['id']}
            IMMICH_HOST_MACHINE_LEARNING=127.0.0.1
            IMMICH_PORT_MACHINE_LEARNING={13000 + inst['id']}
            PUBLIC_IMMICH_SERVER_URL=https://{inst['hostname']}/api
            IMMICH_MACHINE_LEARNING_URL=http://127.0.0.1:{13000 + inst['id']}
            MACHINE_LEARNING_CACHE_FOLDER=/data/immich/{name}/ml_cache
            IMMICH_BUILD_DATA=/opt/immich_data
        """))
        t.service(name=f"immich-server@{name}", state="restarted", enabled=True)
        t.service(name=f"immich-machine-learning@{name}", state="restarted", enabled=True)

