from pyplays import Args, Tasks
import json
import uuid
import argon2
from ansible.template import Templar
from ansible.parsing.dataloader import DataLoader

args = Args(vars, [
    "username",
    ("uid", None),
    "password", # Plain text password for all services
    ("memberOf", []),
    ("permissions", []),
    ("hostedEmails", [])])

# TODO: Do this automagically in pyplays?
templar = Templar(loader=DataLoader(), variables=vars)
username = templar.template(args.username)
uid = templar.template(args.uid)
password = templar.template(args.password)

with Tasks() as t:
    hasher = argon2.PasswordHasher()
    user_json = {
        "memberOf": args.memberOf,
        "permissions": args.permissions,
        "hostedEmails": args.hostedEmails,
        "bindPws": [
            {
                "id": str(uuid.uuid4()),
                "services": "*",
                "hash": hasher.hash(password),
            }
        ],
    }
    if uid is not None:
        user_json["uid"] = int(uid)

    t.copy(f"Create SIDSS RO user {username}",
        dest=f"/data/sidss-mgmt-ro/users/{username}",
        content=json.dumps(user_json))
    t.service("Reload SIDSS",
        name="sidss-mgmt-daemon",
        state="reloaded")
