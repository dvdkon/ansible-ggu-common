import re
from pyplays import _ansible_data, Args, Play

args = Args(vars, [
    "lxd_host", # Name of the host on which LXD is installed
    "host", # Name of the container in LXD and Ansible
    "os", # The dictionary describing the container's source filesystem
          # Can be a shorthand, "alpine", "debian", "opensuse"
    ("idmap_data", False), # Whether to map data UIDs/GIDs into the container
    ("create_datadir", False), # Mount container-specific data directory
    ("create_ssd_datadir", False), # Mount SSD-backed container-specific data directory
    ("mount_userdata", False), # Whether to mount /mnt/main-data/userdata into the container
    ("mount_shared_datadirs", []), # A list of shared datadirs to mount in this container
    ("mount_extra", []), # List of pairs specifying extra bind mounts
    ("extra_devices", {})]) # Map of extra devices to pass (raw LXD config)

bare_os = args.os.split("/")[0]

with Play(args.lxd_host, tags="container-setup", become=True, become_user="root") as p:
    p.assert_("LXD container has ID > 0", that="hostvars[host].id > 0")
    if args.create_datadir:
        p.file(
            "Create container data folder",
            path=f"{{{{ hostvars[lxd_host].containerdata_path }}}}/{args.host}",
            state="directory",
            mode="u=rwx,g=rx,o=rx",
            # Owned by container root (specified by UID)
            owner="{{ hostvars[host].id * 100000 }}",
            group="{{ hostvars[host].id * 100000 }}")

    if args.create_ssd_datadir:
        p.file(
            "Create container SSD data folder",
            path=f"{{{{ hostvars[lxd_host].ssd_containerdata_path }}}}/{args.host}",
            state="directory",
            mode="u=rwx,g=rx,o=rx",
            # Owned by container root (specified by UID)
            owner="{{ hostvars[host].id * 100000 }}",
            group="{{ hostvars[host].id * 100000 }}")

    idmap = ""
    if args.idmap_data:
        idmap += \
        "uid 2000-2999 2000-2999\n" \
        "gid 2000-3999 2000-3999\n"
    if len(args.mount_shared_datadirs) > 0:
        idmap += \
        "uid 4000 4000\n" \
        "gid 4000 4000\n"

    devices = {
        "eth0": {
            "type": "nic",
            "nictype": "bridged",
            "parent": "lxdbr0",
            "ipv4.address": "{{ hostvars[host].ip }}",
            "security.mac_filtering": "true",
            "security.ipv4_filtering": "true",
            "security.ipv6_filtering": "true",
        },
    }
    if args.create_datadir:
        devices["datadisk"] ={
            "type": "disk",
            "source": f"{{{{ hostvars[lxd_host].containerdata_path }}}}/{args.host}",
            "path": "/data"
        }

    if args.create_ssd_datadir:
        devices["ssd_datadisk"] = {
            "type": "disk",
            "source": f"{{{{ hostvars[lxd_host].ssd_containerdata_path }}}}/{args.host}",
            "path": "/ssd_data"
        }
    if args.mount_userdata:
        devices["userdatadisk"] = {
            "type": "disk",
            "source": "{{ hostvars[lxd_host].userdata_path }}",
            "recursive": "true",
            "path": "/userdata"
        }
    for dirname in args.mount_shared_datadirs:
        p.file(
            f"Create shared data directory {dirname}",
            path=f"{{{{ hostvars[lxd_host].shareddata_path }}}}/{dirname}",
            state="directory",
            mode="u=rwx,g=rwxs,o=",
            owner="4000",
            group="4000")
        devices[f"shared_{dirname}"] = {
            "type": "disk",
            "source": f"{{{{ hostvars[lxd_host].shareddata_path }}}}/{dirname}",
            "recursive": "true",
            "path": f"/shareddata/{dirname}"
        }
    for src, dest in args.mount_extra:
        name = re.sub("[^a-zA-Z0-9_-]", "_", src)
        devices[f"extra{src}"] = {
            "type": "disk",
            "source": src,
            "recursive": "true",
            "path": dest,
        }
    for name, vals in args.extra_devices.items():
        devices[name] = vals

    image_alias = args.os
    if args.os == "debian":
        image_alias = "debian/12/amd64"
    elif args.os == "alpine":
        image_alias = "alpine/3.18/amd64"
    elif args.os == "opensuse":
        image_alias = "opensuse/15.5/amd64"
    elif args.os == "alma":
        image_alias = "almalinux/9"
    elif args.os == "openwrt":
        image_alias = "openwrt/22.03"

    source = {
        "type": "image",
        "mode": "pull",
        "protocol":"simplestreams",
        "server": "https://images.linuxcontainers.org",
        "alias": image_alias,
    }

    p.task("community.general.lxd_container",
        "Create LXD container",
        name=args.host,
        source=source,
        config={
            "security.privileged": "false",
            "security.idmap.isolated": "true",
            "security.idmap.base": "{{ hostvars[host].id * 100000 }}",
            "raw.idmap": idmap,
        },
        devices=devices)

    if bare_os == "debian":
        p.raw(f"Wait for internet to be available in in {args.host}",
            "until ping -c1 example.org; do sleep 1; done",
            _delegate_to=args.host)
        p.raw(f"Refresh packages in {args.host}",
            "apt-get update",
            _delegate_to=args.host)
        p.raw(f"Install Python in {args.host}",
            "apt-get install -y python3",
            _delegate_to=args.host)
        # Needed for Ansible's become
        p.raw(f"Install sudo in {args.host}",
            "apt-get install -y sudo",
            _delegate_to=args.host)
    elif bare_os == "alpine":
        p.raw(f"Wait for internet to be available in in {args.host}",
            "until ping -c1 1.1.1.1; do sleep 1; done",
            _delegate_to=args.host)
        p.raw(f"Refresh packages in {args.host}",
            "apk update",
            _delegate_to=args.host)
        p.raw(f"Install Python in {args.host}",
            "apk add python3",
            _delegate_to=args.host)
    elif bare_os == "opensuse":
        p.raw(f"Wait for internet to be available in in {args.host}",
            "until ping -c1 1.1.1.1; do sleep 1; done",
            _delegate_to=args.host)
        p.raw(f"Install Python in {args.host}",
            "zypper install -y python3",
            _delegate_to=args.host)
    elif bare_os == "alma":
        p.raw(f"Wait for internet to be available in in {args.host}",
            "until ping -c1 1.1.1.1; do sleep 1; done",
            _delegate_to=args.host)
        p.raw(f"Install Python in {args.host}",
            "dnf install -y python3",
            _delegate_to=args.host)
    elif bare_os == "openwrt":
        p.raw(f"Wait for internet to be available in in {args.host}",
            "until ping -c1 1.1.1.1; do sleep 1; done",
            _delegate_to=args.host)
        p.raw(f"Install Python in {args.host}",
            "opkg update && opkg install python3",
            _delegate_to=args.host)
        # Without su, `lxc shell` will get confused
        p.raw(f"Install su in {args.host}",
            "opkg install shadow-su",
            _delegate_to=args.host)

    if args.create_datadir:
        # By default LXD creates the directory with 0700 and no native option
        # to change that
        p.raw("Set /data permissions to 0755",
            "chmod 0755 /data",
            _delegate_to=args.host)

with Play(args.host, tags="container-setup") as p:
    if len(args.mount_shared_datadirs) > 0:
        p.group("Add shareddata group",
            name="shareddata",
            state="present",
            gid=4000)
        p.user("Add root to shareddata group",
            name="root",
            append=True,
            groups=["shareddata"])

    if bare_os in ["debian", "alma"]:
        p.import_role(name=f"container_{bare_os}",
            _vars={"lxd_host": args.lxd_host})
